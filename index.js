const path = require('path')
const glob = require('glob')
const electron = require('electron')

const BrowserWindow = electron.BrowserWindow
const app = electron.app

const debug = /--debug/.test(process.argv[2])

if (process.mas) app.setName('Testando Nome')

let mainWindow

function initialize() {
    var shouldQuit = makeSingleIntance()
    if (shouldQuit) return app.quit()

    function createWindow() {
        mainWindow = new BrowserWindow({
            width: 1080,
            minWidth: 600,
            height: 840,
            minHeight: 600,
            title: app.getName()
        })

        mainWindow.loadURL(
            path.join(__dirname, '/index.html')
        )

        if (debug) {
            mainWindow.webContents.openDevTools()
            mainWindow.maximize()

        }
    }

    app.on('ready', function () {
        createWindow()
    })

    app.on('window-all-closed', function () {
        if (process.platform !== 'darwin') {
            app.quit()
        }
    })

    app.on('activate', function () {
        if (mainWindow === null) {
            createWindow()
        }
    })
}


function makeSingleIntance() {
    if (process.mas) return false

    return app.makeSingleInstance(function () {
        if (mainWindow) {
            if (mainWindow.isMinimized()) mainWindow.restore()
            mainWindow.focus()
        }
    })
}

initialize()